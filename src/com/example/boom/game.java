package com.example.boom;

import java.util.Random;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class game extends Activity {

	int rowbases;
	int first;
	int second;
	int third;
	int result;
	int score=0;;
	TextView tv;
	TextView Showscore;
	TextView tvTimer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		final ImageButton R1 = (ImageButton) findViewById(R.id.button1);
		final ImageButton R2 = (ImageButton) findViewById(R.id.button2);
		tv = (TextView) findViewById(R.id.show);
		 tvTimer = (TextView)findViewById(R.id.tvTimer);
	        
	        
	        CountDownTimer cdt = new CountDownTimer(15000, 50) {
	            public void onTick(long millisUntilFinished) {
	            	String strTime = String.format("%.1f" , (double)millisUntilFinished / 1000);
	                tvTimer.setText(String.valueOf(strTime));    
	            }

	            public void onFinish() {
	            	 
	            	
	            	
	            	 Intent goEnd= new Intent(game.this,end.class);
	            	 goEnd.putExtra("sendscore", score);
	            	 startActivity(goEnd);
	            }
	            
	        
	        }.start();
		Showscore = (TextView) findViewById(R.id.tvScore);
		Showscore.setText(String.valueOf(score));
		R1.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {

				// Open Form 2

				rand(1,1);
				rand(2,1);
				rand(3,1);

				tv.setText(String.valueOf(result));
				if (result>=11)
				{
					score++;
				}
				
				Showscore.setText(String.valueOf(score));
			}
		});
		R2.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				// Open Form 2

				rand(1,2);
				rand(2,2);
				rand(3,2);

				tv.setText(String.valueOf(result));
				if (result<11)
				{
					score++;
				}
				Showscore.setText(String.valueOf(score));
			}
		});

	}

	public void rand(int i,int j) {
		// TODO Auto-generated method stub
		Random random = new Random();
		rowbases = random.nextInt(6);

		if (rowbases == 0) {
			if (i == 1) {
				ImageView image1 = (ImageView) findViewById(R.id.imagepic1);

				image1.setImageResource(R.drawable.one);

				image1.setScaleType(ScaleType.FIT_XY);
				first = rowbases + 1;
			} else if (i == 2) {
				ImageView image2 = (ImageView) findViewById(R.id.imagepic2);

				image2.setImageResource(R.drawable.one);

				image2.setScaleType(ScaleType.FIT_XY);
				second = rowbases + 1;
			}

			else if (i == 3) {

				ImageView image3 = (ImageView) findViewById(R.id.imagepic3);

				image3.setImageResource(R.drawable.one);

				image3.setScaleType(ScaleType.FIT_XY);
				third = rowbases + 1;
				
			}
			result = first + second + third;
			
		

		} else if (rowbases == 1) {
			if (i == 1) {
				ImageView image1 = (ImageView) findViewById(R.id.imagepic1);

				image1.setImageResource(R.drawable.two);

				image1.setScaleType(ScaleType.FIT_XY);
				first = rowbases + 1;
			} else if (i == 2) {
				ImageView image2 = (ImageView) findViewById(R.id.imagepic2);

				image2.setImageResource(R.drawable.two);

				image2.setScaleType(ScaleType.FIT_XY);
				second = rowbases + 1;
			} else if (i == 3) {

				ImageView image3 = (ImageView) findViewById(R.id.imagepic3);

				image3.setImageResource(R.drawable.two);

				image3.setScaleType(ScaleType.FIT_XY);
				third = rowbases + 1;
			}
			result = first + second + third;
			

		} else if (rowbases == 2) {
			if (i == 1) {
				ImageView image1 = (ImageView) findViewById(R.id.imagepic1);

				image1.setImageResource(R.drawable.three);

				image1.setScaleType(ScaleType.FIT_XY);
				first = rowbases + 1;
			} else if (i == 2) {
				ImageView image2 = (ImageView) findViewById(R.id.imagepic2);

				image2.setImageResource(R.drawable.three);

				image2.setScaleType(ScaleType.FIT_XY);
				second = rowbases + 1;
			} else if (i == 3) {

				ImageView image3 = (ImageView) findViewById(R.id.imagepic3);

				image3.setImageResource(R.drawable.three);

				image3.setScaleType(ScaleType.FIT_XY);
				third = rowbases + 1;
			}
			result = first + second + third;
			

		} else if (rowbases == 3) {
			if (i == 1) {
				ImageView image1 = (ImageView) findViewById(R.id.imagepic1);

				image1.setImageResource(R.drawable.four);

				image1.setScaleType(ScaleType.FIT_XY);
				first = rowbases + 1;
			} else if (i == 2) {
				ImageView image2 = (ImageView) findViewById(R.id.imagepic2);

				image2.setImageResource(R.drawable.four);

				image2.setScaleType(ScaleType.FIT_XY);
				second = rowbases + 1;
			} else if (i == 3) {

				ImageView image3 = (ImageView) findViewById(R.id.imagepic3);

				image3.setImageResource(R.drawable.four);

				image3.setScaleType(ScaleType.FIT_XY);
				third = rowbases + 1;
			}
			result = first + second + third;
			

		} else if (rowbases == 4) {
			if (i == 1) {
				ImageView image1 = (ImageView) findViewById(R.id.imagepic1);

				image1.setImageResource(R.drawable.five);

				image1.setScaleType(ScaleType.FIT_XY);
				first = rowbases + 1;
			} else if (i == 2) {
				ImageView image2 = (ImageView) findViewById(R.id.imagepic2);

				image2.setImageResource(R.drawable.five);

				image2.setScaleType(ScaleType.FIT_XY);
				second = rowbases + 1;
			} else if (i == 3) {

				ImageView image3 = (ImageView) findViewById(R.id.imagepic3);

				image3.setImageResource(R.drawable.five);

				image3.setScaleType(ScaleType.FIT_XY);
				third = rowbases + 1;
			}
			result = first + second + third;
		

		} else if (rowbases == 5) {
			if (i == 1) {
				ImageView image1 = (ImageView) findViewById(R.id.imagepic1);

				image1.setImageResource(R.drawable.six);

				image1.setScaleType(ScaleType.FIT_XY);
				first = rowbases + 1;
			} else if (i == 2) {
				ImageView image2 = (ImageView) findViewById(R.id.imagepic2);

				image2.setImageResource(R.drawable.six);

				image2.setScaleType(ScaleType.FIT_XY);
				second = rowbases + 1;
			} else if (i == 3) {

				ImageView image3 = (ImageView) findViewById(R.id.imagepic3);

				image3.setImageResource(R.drawable.six);

				image3.setScaleType(ScaleType.FIT_XY);
				third = rowbases + 1;
			}
			result = first + second + third;
			

		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
