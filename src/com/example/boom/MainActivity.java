package com.example.boom;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	String showScore  = "";
	int highS=0;
	String c = "";
	int sendscore = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		File infile = getBaseContext().getFileStreamPath("province.dat");
		if (infile.exists()) {
			try {
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				
				c = reader.readLine().trim();
				highS =Integer.parseInt(c);
				reader.close();
			} catch (Exception e) {
				//Do nothing
				 
			}
		}
		//TextView tv = (TextView)findViewById(R.id.tvScore);
	TextView tvhs = (TextView)findViewById(R.id.tvHS);
		//Bundle bundle = getIntent().getExtras();
		//int ts = bundle.getInt("sendscore");	
		//tv.setText(String.valueOf(ts)); 
		
		//if (highS<ts){
			//highS=ts;
			
		//}
		 c = String.valueOf(highS);
		tvhs.setText(c);
		//save(c);
		
		
		 final ImageButton StartG = (ImageButton) findViewById(R.id.bstart);
		 
		 StartG.setOnClickListener(new View.OnClickListener() {
		       
		        public void onClick(View v) {
		      
		        // Open Form 2
		        
		        Intent newActivity = new Intent(MainActivity.this,game.class);
		        
		        startActivity(newActivity);
		     
		         
		     
		        }
		     
		        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void save(String c){
		try {
			FileOutputStream outfile = openFileOutput("province.dat", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(c);
			p.flush(); p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		}
		
		
		
		
		
	}
	
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(outState != null){
			super.onSaveInstanceState(outState);
		}
		
	}
	
	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
	

}
